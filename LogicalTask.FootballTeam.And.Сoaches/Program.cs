﻿using LogicalTask.FootballTeam.And.Сoaches;

public class Example
{
    public static void Main(string[] args)
    {
        // first example
        var milanAntonio = new Symbol("MilanAntonio");
        var milanRodrigo = new Symbol("MilanRodrigo");
        var milanMykola = new Symbol("MilanMykola");

        var realAntonio = new Symbol("RealAntonio");
        var realRodrigo = new Symbol("RealRodrigo");
        var realMykola = new Symbol("RealMykola");

        var metalistAntonio = new Symbol("MetalistAntonio");
        var metalistRodrigo = new Symbol("MetalistRodrigo");
        var metalistMykola = new Symbol("MetalistMykola");

        var symbols1 = new List<Symbol>
        {
            milanAntonio,
            milanRodrigo,
            milanMykola,
            realAntonio,
            realRodrigo,
            realMykola,
            metalistAntonio,
            metalistRodrigo,
            metalistMykola
        };

        // Not the same nationality of coaches and their teams.
        var notTheSameNationality = new And(
            new Not(milanAntonio),
            new Not(realRodrigo),
            new Not(metalistMykola)
        );

        // Metalist cannot be trained by Antonio.
        var conditionA = new Not(metalistAntonio);

        // Real cannot be trained by Mykola.
        var conditionB = new Not(realMykola);

        var knowledge1 = new And(
            notTheSameNationality,
            conditionA,
            conditionB,
            new Xor(milanAntonio, milanRodrigo, milanMykola),
            new Xor(realAntonio, realRodrigo, realMykola),
            new Xor(metalistAntonio, metalistRodrigo, metalistMykola),
            new Xor(milanMykola, metalistMykola, realMykola),
            new Xor(milanAntonio, metalistAntonio, realAntonio),
            new Xor(milanRodrigo, metalistRodrigo, realRodrigo)
        );

        LogicalModelChecker.CheckKnowledge(knowledge1, symbols1);

        // second example
        var milan = new Symbol("Milan");
        var real = new Symbol("Real");
        var metalist = new Symbol("Metalist");
        var antonio = new Symbol("Antonio");
        var rodrigo = new Symbol("Rodrigo");
        var mykola = new Symbol("Mykola");

        var symbols2 = new List<Symbol>
        {
            milan,
            real,
            metalist,
            antonio,
            rodrigo,
            mykola
        };

        var knowledge2 = new And(
            new Implication(milan, new Not(antonio)),
            new Implication(real, new Not(rodrigo)),
            new Implication(metalist, new Not(mykola)),
            new Implication(metalist, new Not(antonio)),
            new Implication(real, new Not(mykola)),
            new Xor(milan, real, metalist),
            new Xor(antonio, rodrigo, mykola),
            metalist
        );

        LogicalModelChecker.CheckKnowledge(knowledge2, symbols2);
    }
}
