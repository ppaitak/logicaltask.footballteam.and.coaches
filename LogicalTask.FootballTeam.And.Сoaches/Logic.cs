﻿namespace LogicalTask.FootballTeam.And.Сoaches;

public class EvaluationException : Exception
{
    public EvaluationException(string message) : base(message) { }
}

public abstract class Sentence
{
    public abstract bool Evaluate(Dictionary<string, bool> model);
    public abstract string Formula();
    public abstract HashSet<string> Symbols();

    public static void Validate(Sentence sentence)
    {
        if (sentence == null)
        {
            throw new ArgumentNullException(nameof(sentence));
        }
        if (!(sentence is Sentence))
        {
            throw new InvalidCastException("Must be a logical sentence");
        }
    }

    public static string Parenthesize(string s)
    {
        if (string.IsNullOrEmpty(s) || s.All(char.IsLetter) ||
            (s[0] == '(' && s[^1] == ')' && Balanced(s[1..^1])))
        {
            return s;
        }
        else
        {
            return $"({s})";
        }
    }

    private static bool Balanced(string s)
    {
        int count = 0;
        foreach (var c in s)
        {
            if (c == '(')
            {
                count++;
            }
            else if (c == ')')
            {
                if (count <= 0)
                {
                    return false;
                }
                count--;
            }
        }
        return count == 0;
    }
}

public class Symbol : Sentence
{
    private readonly string name;

    public Symbol(string name)
    {
        this.name = name;
    }

    public override bool Evaluate(Dictionary<string, bool> model)
    {
        if (model.TryGetValue(name, out bool value))
        {
            return value;
        }
        throw new EvaluationException($"Variable {name} not in model");
    }

    public override string Formula()
    {
        return name;
    }

    public override HashSet<string> Symbols()
    {
        return new HashSet<string> { name };
    }
}

public class Not : Sentence
{
    private readonly Sentence operand;

    public Not(Sentence operand)
    {
        Sentence.Validate(operand);
        this.operand = operand;
    }

    public override bool Evaluate(Dictionary<string, bool> model)
    {
        return !operand.Evaluate(model);
    }

    public override string Formula()
    {
        return "¬" + Sentence.Parenthesize(operand.Formula());
    }

    public override HashSet<string> Symbols()
    {
        return operand.Symbols();
    }
}

public class And : Sentence
{
    private readonly List<Sentence> conjuncts;

    public And(params Sentence[] conjuncts)
    {
        this.conjuncts = new List<Sentence>(conjuncts);
    }

    public override bool Evaluate(Dictionary<string, bool> model)
    {
        return conjuncts.All(conjunct => conjunct.Evaluate(model));
    }

    public override string Formula()
    {
        return "(" + string.Join(" ∧ ", conjuncts.Select(c => Sentence.Parenthesize(c.Formula()))) + ")";
    }

    public override HashSet<string> Symbols()
    {
        return conjuncts.SelectMany(c => c.Symbols()).ToHashSet();
    }
}

public class Or : Sentence
{
    private readonly List<Sentence> disjuncts;

    public Or(params Sentence[] disjuncts)
    {
        this.disjuncts = new List<Sentence>(disjuncts);
    }

    public override bool Evaluate(Dictionary<string, bool> model)
    {
        return disjuncts.Any(disjunct => disjunct.Evaluate(model));
    }

    public override string Formula()
    {
        return "(" + string.Join(" ∨ ", disjuncts.Select(d => Sentence.Parenthesize(d.Formula()))) + ")";
    }

    public override HashSet<string> Symbols()
    {
        return disjuncts.SelectMany(d => d.Symbols()).ToHashSet();
    }
}

public class Xor : Sentence
{
    private readonly List<Sentence> sentences;

    public Xor(params Sentence[] sentences)
    {
        this.sentences = new List<Sentence>(sentences);
    }

    public override bool Evaluate(Dictionary<string, bool> model)
    {
        return sentences.Count(sentence => sentence.Evaluate(model)) == 1;
    }

    public override string Formula()
    {
        return "(" + string.Join(" ^ ", sentences.Select(sentence => sentence.Formula())) + ")";
    }

    public override HashSet<string> Symbols()
    {
        return sentences.SelectMany(sentence => sentence.Symbols()).ToHashSet();
    }
}

public class Implication : Sentence
{
    private readonly Sentence antecedent;
    private readonly Sentence consequent;

    public Implication(Sentence antecedent, Sentence consequent)
    {
        Sentence.Validate(antecedent);
        Sentence.Validate(consequent);
        this.antecedent = antecedent;
        this.consequent = consequent;
    }

    public override bool Evaluate(Dictionary<string, bool> model)
    {
        return !antecedent.Evaluate(model) || consequent.Evaluate(model);
    }

    public override string Formula()
    {
        return Sentence.Parenthesize(antecedent.Formula()) + " => " + Sentence.Parenthesize(consequent.Formula());
    }

    public override HashSet<string> Symbols()
    {
        return antecedent.Symbols().Union(consequent.Symbols()).ToHashSet();
    }
}

public class Biconditional : Sentence
{
    private readonly Sentence left;
    private readonly Sentence right;

    public Biconditional(Sentence left, Sentence right)
    {
        Sentence.Validate(left);
        Sentence.Validate(right);
        this.left = left;
        this.right = right;
    }

    public override bool Evaluate(Dictionary<string, bool> model)
    {
        return (left.Evaluate(model) && right.Evaluate(model)) || (!left.Evaluate(model) && !right.Evaluate(model));
    }

    public override string Formula()
    {
        return Sentence.Parenthesize(left.Formula()) + " <=> " + Sentence.Parenthesize(right.Formula());
    }

    public override HashSet<string> Symbols()
    {
        return left.Symbols().Union(right.Symbols()).ToHashSet();
    }
}

public static class LogicalModelChecker
{
    public static void CheckKnowledge(Sentence knowledge, List<Symbol> symbols)
    {
        foreach (var symbol in symbols)
        {
            if (ModelCheck(knowledge, symbol))
            {
                Console.WriteLine($"{symbol.Formula()}: Yes");
            }
            else if (!ModelCheck(knowledge, new Not(symbol)))
            {
                Console.WriteLine($"{symbol.Formula()}: Maybe");
            }
        }
    }

    public static bool ModelCheck(Sentence knowledge, Sentence query)
    {
        HashSet<string> symbols = new HashSet<string>(knowledge.Symbols().Union(query.Symbols()));

        return CheckAll(knowledge, query, symbols, new Dictionary<string, bool>());
    }

    private static bool CheckAll(Sentence knowledge, Sentence query, HashSet<string> symbols, Dictionary<string, bool> model)
    {
        if (!symbols.Any())
        {
            if (knowledge.Evaluate(model))
            {
                return query.Evaluate(model);
            }
            return true;
        }
        else
        {
            var remaining = new HashSet<string>(symbols);
            string p = remaining.First();
            remaining.Remove(p);

            var modelTrue = new Dictionary<string, bool>(model) { [p] = true };
            var modelFalse = new Dictionary<string, bool>(model) { [p] = false };

            return CheckAll(knowledge, query, remaining, modelTrue) && CheckAll(knowledge, query, remaining, modelFalse);
        }
    }
}
